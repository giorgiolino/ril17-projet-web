<?php

class Model_ModelUser extends Model_Model
{
    public function updateUser($data){
        $user = $_SESSION['USER'];
        $id = $user->getId();
        $nom = $data['NOM'];
        $prenom = $data['PRENOM'];

        $dt = \DateTime::createFromFormat('d/m/Y', $data['DT_NAIS']);
        $dtNais = $dt->format('Y-m-d');
        
        $sql = ' UPDATE T_USER SET NOM = ?, PRENOM = ?, DT_NAIS = ? WHERE ID_USER = ?';
        
        $statut = false;
        if($stmt = $this->db->prepare($sql)){
            $stmt->bind_param('sssi', $nom, $prenom, $dtNais, $id);
            
            $stmt->execute();
            $statut = true;
            
            $user->setNom($nom)
                ->setPrenom($prenom)
                ->setDateNaissance($dtNais);
        } else {
            $error = $this->db->errno . ' ' . $this->db->error;
        }
        
        return $statut;
    }
    
    public function updateDtCo(){
        $user = $_SESSION['USER'];
        $id = $user->getId();
        
        $sql = ' UPDATE T_USER SET DT_LAST_CO = now() WHERE ID_USER = ?';
        
        $statut = false;
        if($stmt = $this->db->prepare($sql)){
            $stmt->bind_param('i', $id);
            
            $stmt->execute();
            $statut = true;
            
            $user->setDateDerniereConnection(date("Y-m-d H:i:s"));
        } else {
            $error = $this->db->errno . ' ' . $this->db->error;
        }
    }
    
    public function updateDprt($idUser, $idDprt){
        $sql = ' UPDATE T_USER SET FKID_DPRT = ? WHERE ID_USER = ?';
        
        $statut = false;
        if($stmt = $this->db->prepare($sql)){
            $stmt->bind_param('ii', $idDprt, $idUser);
            
            $stmt->execute();
            $statut = true;
        } else {
            $error = $this->db->errno . ' ' . $this->db->error;
        }
    }
    
    public function getUserByDprt($idDprt){
        $res = $this->db->query("
            SELECT
                USER.ID_USER,
                USER.NOM,
                USER.PRENOM,
                USER.DT_NAIS,
                USER.DT_LAST_CO,
                USER.PSEUDO,
                ROLE.NAME ROLE,
                DPRT.NOM DPRT
            FROM T_USER USER
                INNER JOIN T_ROLE ROLE
                    ON ROLE.ID_ROLE = USER.FKID_ROLE
                INNER JOIN T_DPRT DPRT
                    ON DPRT.ID_DPRT = USER.FKID_DPRT
            WHERE USER.FKID_DPRT = {$idDprt}");
        
        $return = array();
        
        if($res->num_rows){
            $row = $res->fetch_all();
            
            foreach($row as $dataUsers){
                $user = new Class_User($dataUsers[0]);
                $user   ->setNom($dataUsers[1])
                        ->setPrenom($dataUsers[2])
                        ->setDateDerniereConnection($dataUsers[4])
                        ->setDateNaissance($dataUsers[3])
                        ->setPseudo($dataUsers[5])
                        ->setRole($dataUsers[6])
                        ->setDepartement($dataUsers[7]);
                $return[] = $user;
            }
        }
        return $return;          
    }
    
    public function getDprt(){
        $res = $this->db->query("
            SELECT
               *
            FROM T_DPRT DPRT");
        
        $return = array();
        
        if($res->num_rows){
            $row = $res->fetch_all();
            foreach($row as $dataDprt){
                $dprt = new Class_Departement($dataDprt[0]);
                $dprt->setNom($dataDprt[1]);
                
                $return[] = $dprt;
            }
        }
        return $return;
    }
}