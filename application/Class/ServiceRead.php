<?php

class Class_ServiceRead
{

    public function __construct($nomService, $nomUser, $dateLecture)
    {
        $this->nomService = $nomService;
        $this->nomUser = $nomUser;
        $this->dateLecture = $dateLecture;
    }

    private $nomService;

    private $nomUser;

    private $dateLecture;

    /**
     *
     * @return mixed
     */
    public function getNomService()
    {
        return $this->nomService;
    }

    /**
     *
     * @return mixed
     */
    public function getNomUser()
    {
        return $this->nomUser;
    }

    /**
     *
     * @return mixed
     */
    public function getDateLecture()
    {
        return date('d/m/Y  H:i:s', strtotime($this->dateLecture));
    }
}