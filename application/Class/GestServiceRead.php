<?php

class Class_GestServiceRead
{
    public function __construct()
    {
        $modelService = new Model_ModelService();
        $this->listeServiceRead = $modelService->getAllServiceRead();
    }
    
    private $listeServiceRead = array();
    /**
     * @return multitype:
     */
    public function getListeServiceRead()
    {
        return $this->listeServiceRead;
    }

    /**
     * @param multitype: $listeServiceRead
     */
    public function setListeServiceRead($listeServiceRead)
    {
        $this->listeServiceRead = $listeServiceRead;
    }

    
}