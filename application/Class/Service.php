<?php

class Class_Service
{

    public function __construct($id)
    {
        $this->id = $id;  
        $model = new Model_ModelService();
        $this->currentUserAgreeDate = $model->isReadByUser($id);
    }

    private $id;
    
    private $nom;

    private $logoUrl;

    private $listePointService = array();

    private $nbUserAgree;

    private $currentUserAgreeDate;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     *
     * @return mixed
     */
    public function getLogoUrl()
    {
        return $this->logoUrl;
    }

    /**
     *
     * @return multitype:
     */
    public function getListePointService()
    {
        return $this->listePointService;
    }

    /**
     *
     * @return mixed
     */
    public function getNbUserAgree()
    {
        if($this->nbUserAgree == null){
            $model = new Model_ModelService();   
            $this->nbUserAgree = $model->getNbUserForService($this->id);
        }        
        
        return $this->nbUserAgree;
    }

    /**
     *
     * @return mixed
     */
    public function getCurrentUserAgreeDate()
    {        
        if(isset($this->currentUserAgreeDate)){
            return date('d/m/Y', strtotime($this->currentUserAgreeDate));
        }
        
        return null;
    }

    /**
     *
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
        return $this;
    }

    /**
     *
     * @param mixed $logoUrl
     */
    public function setLogoUrl($logoUrl)
    {
        $this->logoUrl = $logoUrl;
        return $this;
    }

    /**
     *
     * @param multitype: $listPointService
     */
    public function setListePointService($listPointService)
    {
        $this->listePointService = $listPointService;
        return $this;
    }

    /**
     *
     * @param mixed $nbUserAgree
     */
    public function setNbUserAgree($nbUserAgree)
    {
        $this->nbUserAgree = $nbUserAgree;
        return $this;
    }

    /**
     *
     * @param mixed $currentUserAgree
     */
    public function setCurrentUserAgreeDate($currentUserAgreeDate)
    {
        $this->currentUserAgreeDate = $currentUserAgreeDate;
        return $this;
    }
    
    public function loadService(){
        $modelService = new Model_ModelService();
        $this->listePointService = $modelService->getPointServiceByIdService($this->id);
    }
    
}