<?php

class Controller_AuthController
{

    public function __construct()
    {}

    public function indexAction()
    {
        echo View_AuthView::renderView();
    }
    
    public function checkAuthAction(){
        $pseudo = $_POST['pseudo'];
        $pswd = hash('sha256', $_POST['password']);
        
        $model = new Model_ModelAuth();
        
        $state = $model->checkUser($pseudo, $pswd);
        
        if($state){
            $user = $_SESSION['USER'];
            $modelUser = new Model_ModelUser();
            $modelUser->updateDtCo();
            //connexion OK
            if($user->getRole() == "ROLE_ADMIN"){
                header('Location: /ril17-projet-web/Admin'); 
            }
            else{
                header('Location: /ril17-projet-web/User');
            }
        }
        else{
            //connexion KO
            header('Location: /ril17-projet-web/User'); 
        }
    }
}