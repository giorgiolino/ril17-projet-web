<?php

class View_AuthView{
    
    public static function renderView(){
        $html = "
            <html>
                <head>
                    <link rel='stylesheet'
                    	href='/ril17-projet-web/public/bootstrap/css/bootstrap.min.css'>
                    <link rel='stylesheet'
                    	href='/ril17-projet-web/public/fontawesome/css/all.css'>
                    <link rel='stylesheet'
                    	href='/ril17-projet-web/public/jquery-ui/jquery-ui.min.css'>
                    <link rel='stylesheet'
                    	href='/ril17-projet-web/public/css/auth.css'>
                </head>
                <body id='LoginForm'>
                    <div class='container'>
                        <div class='login-form'>
                            <div class='main-div'>
                                <div class='panel'>
                                    <h2>Authentification</h2>
                                    <p>Merci d'entrer vos identifiant</p>
                                    </div>
                                    <form id='Login' method='post' action='/ril17-projet-web/Auth/checkAuth'>
                                        <div class='form-group'>
                                            <label for='pseudo'>Pseudo : </label>
                                            <input type='text' class='form-control' name='pseudo' placeholder='Pseudo'>
                                        </div>
                                        <div class='form-group'>
                                            <label for='password'>Mot de passe : </label>
                                            <input type='password' class='form-control' name='password' placeholder='Mot de passe'>
                                        </div>
                                        <button type='submit' class='btn btn-primary'>Login</button>
                                    </form>
                                </div>
                            </div>      
                        </div>
                    </div>
                </body>
            	<script src='/ril17-projet-web/public/jquery-3.3.1.min.js'></script>
            	<script src='/ril17-projet-web/public/jquery-ui/jquery-ui.min.js'></script>
            	<script src='/ril17-projet-web/public/bootstrap/js/bootstrap.min.js'></script>
            	<script src='/ril17-projet-web/public/js/script.js'></script>
            </html>
        ";
        return $html;
    }
    
}