SET FOREIGN_KEY_CHECKS = 0;

--	DEPARTEMENT
TRUNCATE TABLE `T_DPRT`;
INSERT INTO `T_DPRT` (`ID_DPRT`, `NOM`) VALUES (NULL, 'Comptabilité'), (NULL, 'Marketing'), (NULL, 'Ressource Humaine'), (NULL, 'Production'), (NULL, 'Communication');

--	ROLE
TRUNCATE TABLE `T_ROLE`;
INSERT INTO `T_ROLE` (`ID_ROLE`, `NAME`) VALUES (NULL, 'ROLE_USER'), (NULL, 'ROLE_ADMIN');

--  USER
TRUNCATE TABLE `T_USER`;

INSERT INTO `T_USER` (`ID_USER`, `NOM`, `PRENOM`, `DT_NAIS`, `DT_LAST_CO`, `PSEUDO`, `PASSWORD`, `FKID_DPRT`, `FKID_ROLE`) VALUES 
(NULL, 'STURM', 'Guillaume', '1991-08-21 00:00:00', '2018-11-14 00:00:00', 'guiz', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '4', '2'),
(NULL, 'SCHALL', 'Anthony', '1989-10-14 00:00:00', '2018-11-14 00:00:00', 'anto', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '3', '2'),
(NULL, 'HONORATO', 'Charles', '1981-05-16 22:40:56', '2018-11-14 00:00:00', 'honor', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '2', '1'),
(NULL, 'HUMBERT', 'Tanisha', '1984-04-29 23:45:28', '2018-11-14 00:00:00', 'tanisha', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '1', '1'),
(NULL, 'JACKSON', 'Robert', '1965-05-08 05:38:08', '2018-11-14 00:00:00', 'five', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '4', '1'),
(NULL, 'GIRARD', 'Jordan', '1983-08-27 05:42:52', '2018-11-14 00:00:00', 'jordan', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '5', '1'),
(NULL, 'PRINCE', 'Alan', '1999-08-18 02:49:37', '2018-11-14 00:00:00', 'alan', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '5', '2'),
(NULL, 'MCLEAN', 'Henry', '1963-01-10 01:56:33', '2018-11-14 00:00:00', 'henry', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '2', '1'),
(NULL, 'ENGLISH', 'Johnny', '1991-08-21 00:00:00', '2018-11-14 00:00:00', 'johnny', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '1', '2'),
(NULL, 'HARLEY', 'Davidson', '1996-05-04 18:01:49', '2018-11-14 00:00:00', 'biker', '8d969eef6ecad3c29a3a629280e686cf0c3f5d5a86aff3ca12020c923adc6c92', '3', '1');

SET FOREIGN_KEY_CHECKS = 1;