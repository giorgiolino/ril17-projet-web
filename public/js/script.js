function ajaxUpdateDprt(e){
    idUser = $(e).data('id');
    idDprt = $(e).parent().find('select').val();
    
    $.ajax({
        url: '/ril17-projet-web/User/updateDepartement',
        data: {
            idDprt: idDprt,
            idUser: idUser
        },
        success: function() {
            $(e).removeClass('btn-primary').addClass('btn-success');
        },
        type: 'POST'
    });
}

function openServiceDetail(e){
    idService = $(e).data('id');
    
    $.ajax({
        url: '/ril17-projet-web/Service/detail',
        data: {
            idService: idService
        },
        success: function(data) {
            var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0) * 80 / 100;
            var h = Math.max(document.documentElement.clientHeight, window.innerHeight || 0) * 80 / 100;

            myModal = new jBox('Modal', {
                content: data,
                maxWidth : w,
                maxHeight : h
            });
 
            myModal.open();
        },
        type: 'POST'
    });
}

function readMention(e){
    idService = $(e).data('id');
    
    $.ajax({
        url: '/ril17-projet-web/Service/readService',
        data: {
            idService: idService
        },
        type: 'POST'
    });
    
    myModal.close();
}

function filterService(e){

    var el = $(e);
    
    if(el.hasClass('btn-success')){
        el.removeClass('btn-success').addClass('btn-danger');
    }
    else{
        el.addClass('btn-success').removeClass('btn-danger');
    }

    applyFilter();
}

function filterAll(e){

    var el = $(e);
    
    if(el.hasClass('btn-success')){
        el.removeClass('btn-success').addClass('btn-danger');
        $('.filterService').each(function(){
            $(this).addClass('btn-danger').removeClass('btn-success');
        })
    }
    else{
        el.addClass('btn-success').removeClass('btn-danger');
        $('.filterService').each(function(){
            $(this).addClass('btn-success').removeClass('btn-danger');
        })
    }

    applyFilter();
}

function applyFilter(){
    $('textFilter').val('');
    let filter = [];
    $('.filterService.btn-success').each(function(){
        filter.push($(this).text());
    })
    
    $('tr').show();
    $('tr').each(function(){
        let s = ('' + $(this).data('initial')).toUpperCase()
        if($.inArray(s, filter) == -1){
            $(this).hide();
        }
    })
}

function filterNameService(e){
    $('tr').show();

    $('.filterService').each(function(){
        $(this).addClass('btn-success').removeClass('btn-danger');
    })

    if($(e).val().length > 0){
        var rgx = new RegExp( "(?:^|\\s)" + $(e).val().replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&'), "i" );

        $('tr').hide().filter(function(){
            return  rgx.test( $(this).data('name') );
        }).show();
    }
}