<?php
require __DIR__ . '/vendor/autoload.php';
ini_set('max_execution_time', 300);

$router = new AltoRouter();
$router->map('GET|POST', '/ril17-projet-web/Auth/checkAuth', '/', 'checkAuth');
$router->map('GET|POST', '/ril17-projet-web/[a:controller]/[a:action]?/[a:param]?', '/', 'controller_action_route');
$router->map('GET|POST', '/ril17-projet-web/[a:controller]/', '/', 'controller_route');
$match = $router->match();

/*
 *  Initialisation de la session pour l'authentification
 */
session_start();
if(isset($_SESSION['AUTH'])){
    // call closure or throw 404 status
    if ($match && ($match['name'] == "controller_action_route" || $match['name'] == "controller_route") && class_exists("Controller_" . $match['params']['controller'] . 'Controller')) {
        if($match['params']['controller'] == 'Admin' && $_SESSION['USER']->getRole() != 'ROLE_ADMIN'){
            $match['params']['controller'] = 'User';
        }
        
        $_SESSION['CONTROLLER'] = $match['params']['controller'];
        
        // Controller
        $controllerName = "Controller_" . $match['params']['controller'] . 'Controller';
        $controller = new $controllerName();
        
        
        // Action
        $match['params']['action'] = isset($match['params']['action']) ? $match['params']['action'] : 'index';
        $actionName = $match['params']['action'] . "Action";
        
        if (method_exists($controller, $actionName)) {
            
            $viewBag = $controller->$actionName();
            if(isset($viewBag)){
                $layoutEnabled = isset($viewBag['enabledLayout']) ? $viewBag['enabledLayout'] : true;                
                
                if ($layoutEnabled != false) {
                    include 'application/Layout/layout.php';
                }
                else{
                    echo $viewBag['content'];
                }
            }
        } else {
            header($_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
        }
    } else {
        // no route was matched
        header($_SERVER["SERVER_PROTOCOL"] . ' 404 Not Found');
    }
}
else{
    $authController = new Controller_AuthController();
    
    if($match && $match['name'] == "checkAuth") {
        $authController->checkAuthAction();
    }
    else{
        $authController->indexAction();
    }
}